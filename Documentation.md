# TapNation Crosspromo

## Integration Steps

1) **"Install"** or **"Upload"** FG Crosspromo plugin from the FunGames Integration Manager in Unity,or download it from here.

2) Click on the **"Prefabs and Settings"** button from FunGames Integration window to fill up your scene with required components and create the Settings asset.